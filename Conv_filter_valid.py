def conv(signal, a, b):
    signal_len = len(signal)
    signal_start = 0
    a_len = len(a)
    b_len = len(b)
    y = []
    n = 0

    for n in range(signal_len):
        yn = 0.0
        signal_slice = signal[signal_start:n + 1]
        signal_slice.reverse()
        nn = n - 1
        y_slice = y[signal_start:nn+1]  # must substrait (signal * a) on the second part, not third
        y_slice.reverse()
        for j in range(a_len):
            if n - j >= 0:
                yn += signal_slice[j] * b[j] # in this part added signal * b coefficient, not a!
            else:
                break
        for j in range(b_len):
            if nn - j >= 0 and len(y_slice) > j:
                yn -= y_slice[j] * a[j]  # in this part added signal * a coefficient, not b!
            else:
                break
        y.append(yn)

    return y

с = conv([1.048, -0.261, -1.959, -0.375, 1.529, -1.228, 1.817, 0.705, 0.724, 0.996], [-2, 1, 1, -2, -1], [2, 3, 1, 5, -2])
print(с)

