import sys
from dataclasses import dataclass

from scipy.fft import fft
import numpy as np
from random import randint


@dataclass
class Params:
    fft_result_file: str = "fft.txt"
    fir_result_file: str = "fir.txt"
    variants_limit: int = 60
    fft_data_length: int = 4
    fir_data_length_min: int = 2
    fir_data_length_max: int = 5


@dataclass
class FFTDataLine:
    variant: int
    data: list[int]


class DataGenerator:
    def generate_fft_tasks(self):
        with open(Params.fft_result_file, 'w') as f:
            for data in self.fft_data_fabric():
                x = np.array(data.data)
                y = fft(x)
                print('Вариант', data.variant, 'исходные данные', data.data, 'результат преобразования', y)
                f.write(f'Вариант № {data.variant}, исходные данные: {data.data},  результат преобразования: {y} \n')

    @staticmethod
    def fft_data_fabric():
        for variant in range(1, Params.variants_limit + 1):
            yield FFTDataLine(variant=variant, data=[randint(-5, 5) for _ in range(Params.fft_data_length)])


def main():
    data_generator = DataGenerator()
    data_generator.generate_fft_tasks()


if __name__ == "__main__":
    main()
