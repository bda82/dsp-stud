from dataclasses import dataclass
from typing import Generator

from scipy.fft import fft
import numpy as np
from random import randint, uniform


@dataclass(frozen=True)
class Params:
    fft_result_file: str = "fft.txt"
    fir_result_file: str = "fir.txt"
    variants_limit: int = 60
    fft_data_length: int = 4
    fft_data_value_min: int = -5
    fft_data_value_max: int = 5
    fir_data_length: int = 10
    fir_data_length_min: int = 2
    fir_data_length_max: int = 5
    fir_value_min: float = -2.0
    fir_value_max: float = 2.0


@dataclass
class FFTDataLine:
    variant: int
    data: list[int]
    result: np.array


@dataclass
class FIRDataLine:
    variant: int
    signal: list[float]
    a: list[float]
    b: list[float]
    result: np.array


class DataGenerator:
    def generate_fft_tasks(self) -> None:
        with open(Params.fft_result_file, 'w') as f:
            for data in self.fft_data_fabric():
                print(f'Вариант № {data.variant}, исходные данные: {data.data},  результат преобразования: {data.result} \n')
                f.write(f'Вариант № {data.variant}, исходные данные: {data.data},  результат преобразования: {data.result} \n')

    @staticmethod
    def fft_data_fabric() -> Generator[FFTDataLine, None, None]:
        for variant in range(1, Params.variants_limit + 1):
            source = [randint(Params.fft_data_value_min, Params.fir_data_length_max) for _ in range(Params.fft_data_length)]
            x = np.array(source)
            y = fft(x)
            yield FFTDataLine(
                variant=variant,
                data=source,
                result=y
            )

    def generate_fir_tasks(self) -> None:
        with open(Params.fft_result_file, 'w') as f:
            for data in self.fir_data_fabric():
                print(data)

    def fir_data_fabric(self) -> Generator[FIRDataLine, None, None]:
        for variant in range(1, Params.variants_limit + 1):
            a_limit = randint(Params.fir_data_length_min, Params.fir_data_length_max)
            b_limit = randint(Params.fir_data_length_min, Params.fir_data_length_max)
            a = [randint(-5, 5) for _ in range(a_limit)]
            b = [randint(-5, 5) for _ in range(b_limit)]
            signal = [uniform(Params.fir_value_min, Params.fir_value_max) for _ in range(Params.fir_data_length)]
            y = self.__conv(signal, a, b)
            yield FIRDataLine(
                variant=variant,
                a=a,
                b=b,
                signal=signal,
                result=y
            )

    @staticmethod
    def __conv(signal, a, b):
        signal_len = len(signal)
        signal_start = 0
        a_len = len(a)
        b_len = len(b)
        y = []
        n = 0

        for n in range(signal_len):
            yn = 0.0
            signal_slice = signal[signal_start:n + 1]
            signal_slice.reverse()
            nn = n - 1
            y_slice = y[signal_start:nn]
            y_slice.reverse()
            for j in range(a_len):
                if n - j >= 0:
                    yn += signal_slice[j] * a[j]
                else:
                    break
            for j in range(b_len):
                if nn - j >= 0 and len(y_slice) > j:
                    yn -= y_slice[j] * b[j]
                else:
                    break
            y.append(yn)

        return y


def main():
    data_generator = DataGenerator()
    print("=====FFT TASKS=====")
    data_generator.generate_fft_tasks()
    print("=====FIR TASKS=====")
    data_generator.generate_fir_tasks()


if __name__ == "__main__":
    main()
